<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="hdTop">
				<div class="row">
					<div class="container">
						<p class="col-6">
							<a class="socialico" href="<?php $this->info('fb_link') ?>">f</a>
							<a class="socialico" href="<?php $this->info('tt_link') ?>">l</a>
							<a class="socialico" href="<?php $this->info('yt_link') ?>">x</a>
							<a class="socialico" href="<?php $this->info('rs_link') ?>">r</a>
						</p>
						<p class="col-6"><img class="bg-phone1" src="public/images/common/sprite.png" alt="phone white" /><?php $this->info(["phone","tel","hdPhone"]); ?></p>
					</div>
				</div>
			</div>
			<div class="hdBot">
				<div class="row">
					<nav>
						<a href="#" id="pull"><strong>MENU</strong></a>
						<ul>
							<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li><li class="border">|</li>
							<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">SERVICES</a></li><li class="border">|</li>
							<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">GALLERY</a></li><li class="border">|</li>
							<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about#content">ABOUT US</a></li><li class="border">|</li>
							<li <?php $this->helpers->isActiveMenu("reviews"); ?>><a href="<?php echo URL ?>reviews#content">REVIEWS</a></li><li class="border">|</li>
							<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">CONTACT</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>

	<?php //if($view == "home"):?>
		<div id="banner">
			<div class="row">
				<div class="bnTop">
					<a href="<?php echo URL; ?>">
						<img src="public/images/common/mainLogo.png" alt="<?php $this->info("company_name"); ?>" class="hdLogo">
					</a>
				</div>
				<div class="bnBot">
					<p>Professional Alterations & Tailoring Services</p>
					<a class="btn" href="about#content">LEARN MORE</a>
				</div>
			</div>
		</div>
	<?php //endif; ?>
