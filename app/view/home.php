<div id="services">
	<div class="row">
		<h2>Our <span>Services</span></h2>
		<div class="container">
			<a class="services-box" href="service#content">
				<img src="public/images/content/services-img1.jpg" alt="Services Image 1">
				<p>MULTIPLE DRESS ALTERATIONS & RUSH WORK</p>
			</a>
			<a class="services-box" href="service#content">
				<img src="public/images/content/services-img2.jpg" alt="Services Image 2">
				<p>CUSTOM <br>DRESS MAKING</p>
			</a>
			<a class="services-box" href="service#content">
				<img src="public/images/content/services-img3.jpg" alt="Services Image 3">
				<p>BRIDAL ALTERATIONS</p>
			</a>
			<a class="services-box" href="service#content">
				<img src="public/images/content/services-img4.jpg" alt="Services Image 4">
				<p>MEN SUIT & TUXEDO ALTERATION</p>
			</a>
		</div>
	</div>
</div>
<div id="welcome">
	<div class="row">
		<div class="container">
			<div class="wlcLeft">
				<div class="text">
					<h2>Welcome</h2>
					<p>Kahala Fashion and Alterations welcomes you to visit our new location. Our staff provides professional tailoring and alterations with high workmanship standards. At Kahlala Fashion and Alterations, we focus on quality, value, service, and customer satisfaction. We provide same day alterations and rush services. No job is too big for Kahlala Fashion and Alterations.  </p>
					<p><img class="bg-phone1" src="public/images/common/sprite.png" alt="phone icon" /><?php $this->info(["phone","tel","wlcPhone"]); ?></p>
				</div>
			</div>
			<img src="public/images/content/welcome-img.jpg" alt="front" class="welcome-img">
		</div>
	</div>
</div>
<div id="about">
	<div class="row">
		<div class="container">
			<img class="about-img" src="public/images/content/about-img.jpg" alt="sewing kit" />
			<div class="aboutRight">
				<div class="text">
					<h2><span>About</span> Us</h2>
					<p>Kahala Fashion and Alteration was formerly known as Waikahala Fashion Center Company which was located in Waialae Center, across the street from Kahala Mall.   With over 30 years of history and service for our customers and friends in Kaimuki, Kahala, and the eastern region of Honolulu.    We have recently relocated our business to Kahala Atrium,    same side with Territorial Saving Bank in same parking lot.    Please visit our new location featuring spacious facilities and 2 large fitting rooms.  Kahala Fashion and Alteration staff will tailor to your needs one stitch at a time. </p>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="gallery">
	<div class="row">
		<div class="container">
			<div class="gallery-img">
				<img src="public/images/content/gallery1.jpg" alt="Gallery Image 1" />
			</div>
			<div class="gallery-img">
				<img src="public/images/content/gallery2.jpg" alt="Gallery Image 2" />
			</div>
			<div class="gallery-img">
				<img src="public/images/content/gallery3.jpg" alt="Gallery Image 3" />
				<div class="cover">
					<div class="text">
						<h2>Our Gallery</h2>
						<a class="btn" href="gallery#content">VIEW MORE</a>
					</div>
				</div>
			</div>
			<div class="gallery-img">
				<img src="public/images/content/gallery4.jpg" alt="Gallery Image 4" />
			</div>
			<div class="gallery-img">
				<img src="public/images/content/gallery5.jpg" alt="Gallery Image 5" />
			</div>
		</div>
	</div>
</div>
<div id="contact">
	<div class="row">
		<div class="container">
			<h2>Contact Us</h2>
			<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
				<div class="contact-container">
					<div class="cntLeft">
						<label><span class="ctc-hide">Name</span>
							<input type="text" name="name" placeholder="Name:">
						</label>
						<label><span class="ctc-hide">Phone</span>
							<input type="text" name="phone" placeholder="Phone:">
						</label>
						<label><span class="ctc-hide">Email</span>
							<input type="text" name="email" placeholder="Email:">
						</label>
					</div>
					<div class="cntRight">
						<label><span class="ctc-hide">Message</span>
							<textarea name="message" cols="30" rows="10" placeholder="Message / Questions:"></textarea>
						</label>
					</div>
				</div>
				<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
				<div class="g-recaptcha"></div>
				<label>
					<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
				</label><br>
				<?php if( $this->siteInfo['policy_link'] ): ?>
				<label>
					<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
				</label>
				<?php endif ?>
				<button type="submit" class="ctcBtn btn" disabled>SUBMIT FORM</button>
			</form>
		</div>
	</div>
</div>
<div id="testimonial">
	<div class="row">
		<div class="container">
			<img class="testimonial-img" src="public/images/content/testimonials-img.jpg" alt="lucky" />
			<div class="testRight">
				<div class="text">
					<h2><span> What</span> They Say</h2>
					<p>
						<span class="star"> &#9733; &#9733; &#9733; &#9733; &#9733;</span>
						“Always pleasant and items are ready when promised. They were able to accommodate our rush request for a coat my husband needed taking in. Very professional. Our go-to place for alterations! Thank you!”
						<span class="auth">- Rachel O.</span>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="cntDetails">
	<div class="row">
		<div class="container">
			<div class="cdLeft">
				<h2><span>Contact</span> Us</h2>
				<p><span><img class="bg-phone2" src="public/images/common/sprite.png" alt="icon" /></span> <span class="txt"><?php $this->info(["phone","tel","botPhone"]); ?></span></p>
				<p><span><img class="bg-email" src="public/images/common/sprite.png" alt="icon" /></span> <span class="txt"><?php $this->info(["email","mailto","botEmail"]); ?></span></p>
				<p><span><img class="bg-clock" src="public/images/common/sprite.png" alt="icon" /></span> <span class="txt"><?php $this->info("hours"); ?></span></p>
			</div>
			<img class="contact-img" src="public/images/content/contact-img.jpg" alt="factory" />
		</div>
	</div>
</div>
